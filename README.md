# AI_DTI : Predicting activatory and inhibitory drug–target interactions based on mol2vec and genetically perturbed transcriptomes

## Overview 

This preject is to develop a framework that predict activatory and inhibitory drug-target intersctions (DTIs).

You can predict new activatory and inhibitory DTIs using the pre-trained model or reproduce our results. 

---

## Requirement
deep-forest (v.1.1.1)
numpy
pandas 
scikit-learn  
rdkit
gensim

---

## Novel DTIs prediction using pretraining model
### Usage
 1. unzip AI_DTI_pretrained.zip in ./data
 2. Run 
 3. 


### Data Specification

### Example
python AI_DTI_pretrained.py --output_file=DTI_result.csv --compound_file=.\pretrained_model\compound_input_example.csv --target_file=.\pretrained_model\target_integrated_dataset.csv


## Reproducing our results
### Usage


### Data specification

### Example
python AI_DTI.py --dataset=ref --MOA=act --neg_ratio=1
